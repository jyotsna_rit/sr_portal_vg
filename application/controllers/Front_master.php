<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_master extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->helper('cookie');	
		$this->load->model('master_model');	
		$this->form_validation->set_message('required','%s');

		$time = time();
		$timeout_duration = 1200;
		 if (($this->session->userdata('user_id')) &&
		 ($time - $this->session->userdata("LAST_ACTIVITY")) > $timeout_duration) {
		    $this->session->unset_userdata(array('user_id','first_name','last_name','user_type','LAST_ACTIVITY'));
			$this->session->sess_destroy();
		}
		$this->session->set_userdata('LAST_ACTIVITY',$time);
	}
	
	public function get_header()
	{		
		$this->load->view('layout/header');
	}
	public function get_user_header_name($user_id)
	{
		$cond=array('user_id'=>$user_id,'user_status' => 1);
		$user_data = $this->master_model->get_one_record("vg_user_master",$cond);
		return $user_data;
	}
	public function get_footer()
	{
		$this->load->view('layout/footer');
	}

	public function check_login()
	{
		if(!$this->session->userdata('user_id'))
		{
			redirect(base_url());
			exit();
		}
		else
		{
			redirect(current_url());
		}
	}

	public function logout() 
	{
 		if($this->session->userdata('user_id'))
 		{  
 			$this->session->unset_userdata(array('user_id','first_name','last_name','user_type','LAST_ACTIVITY'));
 		}
 		else
 		{
 			$this->session->sess_destroy();
 		}
 		redirect(base_url());
    }

	public function do_upload($filename,$path)
	{
			
		$image_path = realpath(APPPATH ."../uploads/".$path);
        $config['upload_path']          = $image_path;
        $config['allowed_types']        = '*';
        $config['max_size']             = 5000000;
        $config['max_width']            = 2000000;
        $config['max_height']           = 76800000;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload($filename))
        {
			$file_data = array('error' => $this->upload->display_errors());
        }
        else
		{ 
			$file_data = array('upload_data' => $this->upload->data());

			// $upload_data=$this->upload->data();
			// if($upload_data['is_image'])
			// {
	  //   		$this->resize_image($upload_data,150,150);
	  //   		$this->resize_image($upload_data,300,300);
   //  		}
        }        
			return $file_data;
    }
    public function resize_image($upload_data,$width,$height)
    {
    	$this->load->library('image_lib');
		$config1 = array(
			'image_library'=>'gd2',
	        'source_image' => $upload_data['full_path'],
	        //'create_thumb'=>'150x150',
	        'new_image' => $upload_data['raw_name']. "-".$width."x".$height.$upload_data['file_ext'],
	        'maintain_ratio' => false,
	        'width' => $width,
	        'height' => $height
    	);
    	//Image Autorotating Resize Orientation Setting
    	$filename = $upload_data['full_path'];
    	// if(extension_loaded("exif"))
    	// {    	
	    	$exif = exif_read_data($filename);
	        if (!empty($exif['Orientation']))
	        {
	            $image = imagecreatefromjpeg($filename);
	           	switch ($exif['Orientation'])
	            {
	                case 3:
	                    $image = imagerotate($image, 180, 0);
	                    break;

	                case 6:
	                    $image = imagerotate($image, -90, 0);
	                    break;

	                case 8:
	                    $image = imagerotate($image, 90, 0);
	                    break;
	            }
	            imagejpeg($image, $filename, 90);
	        }
    	// }
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
    }

    public function address_to_location($address='')
    {
    	$data=array();
    	if(!empty($address))
    	{
    		$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
    		//&key=AIzaSyCQrBBPBxsRK-lVGG4LsJQRtxu81-2qp44'
			$geo = json_decode($geo, true); // Convert the JSON to an array
			$latitude1 = '';
			$longitude1 = '';
			
			if (isset($geo['status']) && ($geo['status'] == 'OK'))
			{
			  	$latitude1 = $geo['results'][0]['geometry']['location']['lat'];  // Latitude
			  	$longitude1 = $geo['results'][0]['geometry']['location']['lng'];
			}

    		$data['latitude']= $latitude1 ;
    		$data['longitude']= $longitude1 ;
    	}
    	return $data;
    }

    public  function send_email($to,$subject,$maildata)
	{
	   	$config = Array(
		  // 'protocol' => 'smtp',
		  // 'smtp_host' => 'ssl://smtp.googlemail.com',
		  // 'smtp_port' => 465,
		  // 'smtp_user' => 'xxx@gmail.com', // change it to yours
		  // 'smtp_pass' => 'xxx', // change it to yours
		  'mailtype' => 'html',
		  //'charset' => 'iso-8859-1',
		  'wordwrap' => TRUE
		);

	    $this->load->library('email', $config);
	    $this->email->set_newline("\r\n");
	    $this->email->from(admin_notification_email,'Venita Group');
	    $this->email->to($to);
	    if(admin_cc_email !="")
		{
	   		$this->email->cc(admin_cc_email);
	   	}
	    $this->email->subject($subject);
	    $this->email->message($maildata);
	    $this->email->set_mailtype("html");
	    if(email_send == "Yes")
	    {
	    	$this->email->send();
	    }
	}

	public function checkpassword()
    {
        $user_id = $this->session->userdata('user_id');
        $password = $this->input->post('sPassword');
        $cond = array("user_id"=>$user_id,'password'=>md5($password),'isDelete' => 0);
        $rec = $this->master_model->get_record('vg_user_master',$cond);
        $results = count($rec);
        if($results > 0){
            $res = "true";
        }else{
            $res = "false";
        }
        echo $res;
    }

    public function update_password()
    {
        $user_id = $this->session->userdata('user_id');
        $password = $this->input->post('sConfirmPassword');
        if(!empty($password))
        {
            $data=array(
                'password'=>md5($password),
            );
            $cond = array("user_id"=>$user_id,'user_status'=> 1,'isDelete' => 0);
            $this->master_model->update_record('vg_user_master',$data,$cond);

            echo json_encode(array("result"=>"success","change"=>"true"));
        }
    }

    public function get_service_request()
    {
        $category_id = $this->input->post("category_id");
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');
          
        $cond = '';
        if(!empty($category_id) && is_array($category_id))
        {
        	$category_id = implode(',', $category_id);
        }
        $cond .= " category_id IN ($category_id)";

        /*$cond_get=array(
            'user_id'=>$user_id,
            'user_status' =>1,
            'isDelete'=>0,
        );
        $filed=array('service_request_id');
        $user_data_master=$this->master_model->get_record_fields('vg_user_master',$filed,$cond_get);
        if(!empty($user_data_master) && $user_data_master[0]->service_request_id)
        {
            $service_request_id  = $user_data_master[0]->service_request_id;
            $cond  .= " AND service_request_id IN ($service_request_id) ";
        }*/
        $service_request_data= $this->master_model->get_record('vg_service_request',$cond);

        $data = '';
        $data .= '<option value="">Select Service Request Type</option>';
        foreach($service_request_data as $service_request_row)
        {   
            $data .= "<option value=".$service_request_row->service_request_id.">".$service_request_row->service_request_name."</option>";
        }
        echo json_encode(array("result"=>"success","data"=>$data));
        exit();
    }
}
?>
