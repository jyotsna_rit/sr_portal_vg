<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'Front_master.php';
class Admin extends Front_master
{
	public function __construct()
	{
		parent:: __construct();
        if ($this->session->userdata('user_id') == '' || $this->session->userdata('user_type') != 'admin')
        { 
            redirect(base_url());
            exit();
        }
    }

    public function index()
    {
        $this->config->config["page_title"]= 'Dashboard';
    	$this->get_header();

        $cond=array('user_type' =>'support','isDelete'=>0);
        $data['support_count'] = $this->master_model->get_count("vg_user_master",$cond);

        $cond=array('user_type' =>'employee','isDelete'=>0);
        $data['employee_count'] = $this->master_model->get_count("vg_user_master",$cond);

        $cond=array('user_type' =>'admin','isDelete'=>0);
        $data['admin_count'] = $this->master_model->get_count("vg_user_master",$cond);

    	$this->load->view('admin/dashbord',$data);
    	$this->get_footer();
    }

    public function all_user()
    {
        $this->config->config["page_title"]= 'All Users';
        $this->get_header();
        $cond=array('isActive' =>1,'isDelete'=>0);   
        $data['location_data']=$this->master_model->get_record('vg_location_master',$cond);
        $this->load->view('admin/all-users',$data);
        $this->get_footer();
    }

    public function getalluser()
    {
        $dynamic_limit = $this->input->post('limit');
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        if($dynamic_limit == '')
        {
            $user_status = $this->input->post('user_status');
            $user_type = $this->input->post('user_type');
            $location_id = $this->input->post('location_id');
            $sKeyword = trim($this->input->post('sKeyword')," ");
            
            $query = '';

            if(!empty($sKeyword))
            {
                $query .= " AND CONCAT(TRIM(first_name), ' ', TRIM(last_name), ' ',emailid,' ',user_name)  LIKE  '%".$sKeyword."%'  ";
            }

            if(!empty($user_status))
            {
                $query .= ' AND user_status IN ('.$user_status.') ';
            } 

            if(!empty($user_type))
            {
                $query .= ' AND user_type IN ('.$user_type.') ';
            }

            if(!empty($location_id))
            {
                $query .= " AND location_id = $location_id ";
            }      

            
            
            $cond = " isDelete = '0' $query ";
            $query = "SELECT count(user_id) as total_count FROM vg_user_master WHERE $cond ";
            $totalData =$this->master_model->custom_query($query);
            
            $total_row = $totalData[0]->total_count;

            // add pagination limit
            $limit = $_POST['length'];
            $start = $_POST['start'];
            $q= '';
            if($columnName != 'no')
            {
                $q = "order by ".$columnName." ".$columnSortOrder." ";
            }
            $cond .= " $q LIMIT $limit  OFFSET $start";
            
            $query1 = "SELECT * FROM vg_user_master WHERE $cond ";
            $user_data = $this->master_model->custom_query($query1);
        }
        else
        {
            $total_row = $dynamic_limit;
            $q= '';
            if($columnName != 'no')
            {
                $q = "order by ".$columnName." ".$columnSortOrder." ";
            }
            else
            {
                $q = " ORDER BY user_id ASC;";
            }

            //$cond = "    ";
            $query1 = "SELECT * FROM (SELECT * FROM vg_user_master WHERE isDelete = '0' order by user_id desc LIMIT $dynamic_limit) t $q ";
            $user_data = $this->master_model->custom_query($query1);
        }
    

        $data = array();
        if(!empty($user_data))
        {
            $i = 1;
            foreach ($user_data as $user_data_row)
            {
                $status = '';
                $class = '';
                if($user_data_row->user_status == 1)
                {
                    $status = 'Active';
                    $class = 'green-text';
                }
                elseif($user_data_row->user_status == 2)
                {
                    $status = 'Inactive';
                    $class  = 'blue-text';
                }
                elseif($user_data_row->user_status == 3)
                {
                    $status = 'Delete';
                    $class  = 'red-text';
                }

                $add_url = '';
                if($status != 'Delete')
                {
                    $add_url = '<a class="action-icon isdelete" href="'.base_url().'admin/delete_user/'.$user_data_row->user_id.'"><i class="venita-delete-icon"></i></a>';
                }

                $url = '<a class="action-icon" href="'.base_url().'admin/create-user/'.$user_data_row->user_id.'"><i class="venita-pencil-icon"></i></a>'.$add_url;

                $email = '<a href="mailto:'.$user_data_row->emailid.'">'.$user_data_row->emailid.'</a>';

                $nestedData = array();
                $nestedData[] = '';
                $nestedData[] = 'VG'.$user_data_row->user_id;
                $nestedData[] = $user_data_row->user_name;
                $nestedData[] = $email;
                $nestedData[] = $user_data_row->user_type;
                $nestedData[] = $this->get_location($user_data_row->location_id);
                $nestedData[] = '<span class="'.$class.'">'.$status.'</span>';
                $nestedData[] = $url;
            
                $data[] = $nestedData;  
                $i++;
            }
        }

        $json_data = array(
                'draw'            => $_POST['draw'],
                'recordsTotal'    => $total_row,
                'recordsFiltered' => $total_row,
                'data'            => $data
        );
        echo json_encode($json_data);
    }

    public function get_location($location_id)
    {
        $cond=array(
            'location_id'=>$location_id,
            'isActive' =>1,
            'isDelete'=>0,
        );
        $filed=array('location_name');
        $location_master=$this->master_model->get_record_fields('vg_location_master',$filed,$cond);
        if(!empty($location_master))
        {
            return $location_master[0]->location_name;
        }
    }

    public function create_user($user_id='')
    {
        $this->config->config["page_title"]= 'Create User';
        $this->get_header();

        $this->form_validation->set_rules('first_name', 'Please enter user name.', 'required');
        $this->form_validation->set_rules('last_name', 'Please enter last name.', 'required');
        $this->form_validation->set_rules('user_name', 'Please enter user name.', 'required');
        if(empty($user_id))
        {
            $this->form_validation->set_rules('password', 'Please enter password.', 'required');
        }
        $this->form_validation->set_rules('location_id', 'Please select location.', 'required');
        $this->form_validation->set_rules('user_type', 'Please select user type.', 'required');


        if ($this->form_validation->run() == FALSE)
        {
            $cond=array('isActive' =>1,'isDelete'=>0);
            $data['category_data']=$this->master_model->get_record('vg_category',$cond);
            $data['service_request_data']=$this->master_model->get_record('vg_service_request',$cond);
            $data['location_data']=$this->master_model->get_record('vg_location_master',$cond);
            
            $this->config->config["page_title"]= 'Create User';  
            if(!empty($user_id))
            {
                $cond=array('user_id'=>$user_id,'isDelete'=>0);
                $data['user_data']=$this->master_model->get_one_record("vg_user_master",$cond);

                $this->config->config["page_title"]= 'Edit Profile Details';  
            }
            
            $this->load->view('admin/create-user',$data);
        }
        else 
        {   
            $emailsend = '';
            $dCreatedDate = date("Y-m-d H:i:s");
            $dModifyDate = date("Y-m-d H:i:s");

            $category_id = '';
            $service_request_id = '';
            if(!empty($this->input->post('category_id')))
            {   
                $category_id = implode(',', $this->input->post('category_id'));
            }
            if(!empty($this->input->post('service_request_id')))
            {   
                $service_request_id = implode(',', $this->input->post('service_request_id'));
            }

            if(!empty($user_id))
            {
                $cond=array('user_id'=>$user_id,'isDelete'=>0);
                $user_data = $this->master_model->get_one_record("vg_user_master",$cond);
                
                $password_get = md5($this->input->post('password'));
                if(!empty($this->input->post('hid_password')) && empty($this->input->post('password')) && !empty($user_data->password))
                {
                    $password_get = $user_data->password;
                }

                $data=array(
                    'first_name'=>$this->input->post('first_name'),
                    'last_name'=>$this->input->post('last_name'),
                    'user_name'=>$this->input->post('user_name'),
                    'emailid'=>$this->input->post('emailid'),
                    'password'=>$password_get,
                    'location_id'=>$this->input->post('location_id'),
                    'user_type'=>$this->input->post('user_type'),
                    'category_id' =>$category_id,
                    'service_request_id' =>$service_request_id,
                    'user_status' => $this->input->post('user_status'), 
                    'dModifyDate' =>$dModifyDate,
                );
                $cond=array(
                    'user_id'=>$user_id,
                );
                $this->master_model->update_record('vg_user_master',$data,$cond);
                
                if(!empty($user_data) && (!empty($this->input->post('password')) && $user_data->password != md5($this->input->post('password'))))
                {
                    $emailsend = 'Yes';
                    $password = $this->input->post('password');
                }    
                
                $this->session->set_flashdata('success_msg','User updated successfully.');
                redirect($this->agent->referrer());      
            }
            else
            {
                $password  = $this->input->post('password');
                $data=array(
                    'first_name'=>$this->input->post('first_name'),
                    'last_name'=>$this->input->post('last_name'),
                    'user_name'=>$this->input->post('user_name'),
                    'emailid'=>$this->input->post('emailid'),
                    'password'=>md5($password),
                    'location_id'=>$this->input->post('location_id'),
                    'user_type'=>$this->input->post('user_type'),
                    'category_id' =>$category_id,
                    'service_request_id' =>$service_request_id,
                    'dCreatedDate' =>$dCreatedDate,
                    'dModifyDate' =>$dModifyDate,
                );
                
                $this->master_model->insert_record('vg_user_master',$data);
                $emailsend = 'Yes';
                $this->session->set_flashdata('success_msg','User successfully added.');    
            }

            if($emailsend == 'Yes')
            {
                $viewData['mail_data']=array(
                    'first_name'=>$this->input->post('first_name'),
                    'user_name'=>$this->input->post('user_name'),
                    'password' => $password,
                );

                $msg_user = $this->load->view('email-templates/welcome-user', $viewData, true);

                //Email Send
                $this->send_email($this->input->post('emailid'),'Welcome To Venita Group',$msg_user);
                
                redirect('admin');
            }
            
        }
        $this->get_footer();
    }

    public function getemailexists()
    {
        $emailid = $this->input->post('emailid');
        $cond = array("emailid"=>$emailid,'isDelete' => 0);
        $rec = $this->master_model->get_record('vg_user_master',$cond);
        $results = count($rec);
        if($results > 0){
            $res = "false";
        }else{
            $res = "true";
        }
        echo $res;
    }

    public function geteusername()
    {
        $user_name = $this->input->post('user_name');
        $cond = array("user_name"=>$user_name,'isDelete' => 0);
        $rec = $this->master_model->get_record('vg_user_master',$cond);
        $results = count($rec);
        if($results > 0){
            $res = "false";
        }else{
            $res = "true";
        }
        echo $res;
    }


    public function get_service_request()
    {
        $category_id = $this->input->post("category_id");
        $category_id = implode(',', $category_id);
        $cond= " category_id IN ($category_id)";
        $service_request_data= $this->master_model->get_record('vg_service_request',$cond);

        $data = '';
        foreach($service_request_data as $service_request_row)
        {   
            $data .= "<option value=".$service_request_row->service_request_id.">".$service_request_row->service_request_name."</option>";
        }
        echo json_encode(array("result"=>"success","data"=>$data));
        exit();
    }

    public function admin_setting()
    {
        $this->config->config["page_title"]= 'Settings';
        $user_id = $this->session->userdata('user_id');

        $this->get_header();
        $this->form_validation->set_rules('first_name', 'Please enter user name.', 'required');
        $this->form_validation->set_rules('last_name', 'Please enter last name.', 'required');
        $this->form_validation->set_rules('user_name', 'Please enter user name.', 'required');
        $this->form_validation->set_rules('location_id', 'Please select location.', 'required');
        if(!$this->input->post('user_type_hid'))
        {
            $this->form_validation->set_rules('user_type', 'Please select user type.', 'required');
        }

        if ($this->form_validation->run() == FALSE)
        {
            $cond=array('isActive' =>1,'isDelete'=>0);
            $data['category_data']=$this->master_model->get_record('vg_category',$cond);
            $data['service_request_data']=$this->master_model->get_record('vg_service_request',$cond);
            $data['location_data']=$this->master_model->get_record('vg_location_master',$cond);

            $cond=array('user_id'=>$user_id,'user_status' => 1,'isDelete'=>0);   
            $data['user_data']=$this->master_model->get_one_record('vg_user_master',$cond);
            $this->load->view('admin/setting',$data);
        }
        else
        { 
            $dCreatedDate = date("Y-m-d H:i:s");
            $dModifyDate = date("Y-m-d H:i:s");

            $category_id = '';
            $service_request_id = '';
            if(!empty($this->input->post('category_id')))
            {   
                $category_id = implode(',', $this->input->post('category_id'));
            }
            if(!empty($this->input->post('service_request_id')))
            {   
                $service_request_id = implode(',', $this->input->post('service_request_id'));
            }
            $back_url = $this->input->post('back_url');
            if(!empty($user_id))
            {
                $data=array(
                    'first_name'=>$this->input->post('first_name'),
                    'last_name'=>$this->input->post('last_name'),
                    'user_name'=>$this->input->post('user_name'),
                    'emailid'=>$this->input->post('emailid'),
                    'location_id'=>$this->input->post('location_id'),
                    'user_type'=>$this->input->post('user_type_hid'),
                    'dModifyDate' =>$dModifyDate,
                );
                $cond=array(
                    'user_id'=>$user_id,
                );
                $this->master_model->update_record('vg_user_master',$data,$cond);

                if(!empty($back_url))
                {
                    $this->session->set_userdata(array('back_url'=>$back_url));
                }
                
                $this->session->set_flashdata('success_msg','Settings successfully updated.');
            }
            redirect($this->agent->referrer());           
        }
        $this->get_footer();
    }

    public function delete_user($delete_id='')
    {
        if(!empty($delete_id))
        {
            $user_id = $this->session->userdata('user_id');
            if($user_id == $delete_id)
            {
                $this->session->set_flashdata('error_msg',"You can't delete your current logged in account.");
            }
            else
            {
                $data=array(
                    'user_status'=>3,
                );
                $cond = array("user_id"=>$delete_id);
                $this->master_model->update_record('vg_user_master',$data,$cond);
                $this->session->set_flashdata('success_msg','User deleted successfully.');
            }
            redirect($this->agent->referrer());
        } 
    }

}
?>