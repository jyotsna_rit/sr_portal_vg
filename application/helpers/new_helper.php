<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// if(!function_exists('isLogin')){
// 	function isLogin()
// 	{
// 		$CI = &get_instance();
// 		if(!$CI->session->userdata('user_id'))
// 		{
// 			redirect(base_url());
// 			exit();
// 		}
// 		else
// 		{
// 			redirect(current_url());
// 		}
// 	}
// }


if(!function_exists('safe_b64encode')){
	function safe_b64encode($string){
		$data = base64_encode($string);
		$data = str_replace(array('+','/','='),array('-','_',''),$data);
		return $data;
	}
}

if(!function_exists('safe_b64decode')){
	function safe_b64decode($string){
		$data = str_replace(array('-','_'),array('+','/'),$string);
		$mod4 = strlen($data) % 4;
		if ($mod4) {
			$data .= substr('====', $mod4);
		}
		return base64_decode($data);
	}
}


if(!function_exists('random_code')){
	function random_code($length_of_string) 
	{
	    // String of all alphanumeric character 
	    $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
	    // Shufle the $str_result and returns substring 
	    // of specified length 
	    return substr(str_shuffle($str_result),  
	                       0, $length_of_string); 
	} 
}

if(!function_exists('send_email')){
	function send_email($to,$subject,$maildata){

		$ci = & get_instance();
		$config = Array(
			//'protocol' => 'smtp',
			//'smtp_host' => 'ssl://smtp.googlemail.com',
			//'smtp_port' => 465,
			//'smtp_user' => 'abc@gmail.com', 
			//'smtp_pass' => 'passwrd', 
			'mailtype' => 'html',
			//'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$ci->load->library('email');
	    $ci->email->from($ci->config->item('admin_notification_email'), 'Venita Group');
	    $ci->email->to($to);
	    if($ci->config->item('admin_cc_email') !='')
	    {
	    	$ci->email->cc($ci->config->item('admin_cc_email'));
	    }
	    $ci->email->subject($subject);
	    $ci->email->message($maildata);
	    if($ci->config->item('email_send') == "Yes")
	    {
	    	$r = $ci->email->send();
			// if (!$r) {
			//   ob_start();
			//   $this->email->print_debugger();
			//   $error = ob_end_clean();
			//   return $error;
			// }
	    }
	}
}


if(!function_exists("setCurrentUrl")){
	function setCurrentUrl(){
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
		{   
			$link = "https";
		} 
		else{
			$link = "http"; 
		}
		$link .= "://"; 
		$link .= $_SERVER['HTTP_HOST']; 
		$link .= $_SERVER['HTTP_REFERER'];
		
		return $link;
	}
}
