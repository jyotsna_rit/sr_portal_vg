<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php
	$site_name='';
	if(!empty($this->config->config["site_name"])){
	    $site_name=$this->config->config["site_name"];
	}
	else{
	    $site_name='Venita Group';
	}

	$title=$site_name;
	$page_title='';
	if(!empty($this->config->config["page_title"]))
	{
	    $title=$title.' | '.$this->config->config["page_title"];
	    $page_title=$this->config->config["page_title"];
	}
?>
    <title><?php echo $title; ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/open-sans/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/fontawesome-5/css/all.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/venita-icons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/datatables.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/select2.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/responsive.css">
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
</head>
<body>
    <?php 
        if (!empty($this->session->userdata('user_id')))
        {
            $CI =& get_instance();
    ?>
	<header class="site-header">
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="<?php echo base_url() ?>">
                        <div class="site-identity">
                            <img src="<?php echo base_url() ?>assets/images/venita-logo.png" alt="logo">
                        </div>
                    </a>
                    <button class="navbar-toggler menu_toggle" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse main-menu navbar-collapse nav_menu" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <?php if($this->session->userdata('user_type') == 'employee' || $this->session->userdata('user_type') == 'support'){ 

                            $notification_data = $CI->notificationcount();
                            if(!empty($notification_data))
                            {
                                $total_count = count($notification_data);
                            }
                        ?> 
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url('create-request'); ?>">Create New SR</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo base_url('service-request'); ?>">My Service Requests</a>
                        </li>
                        <li id="noti_dropdown_id" class="nav-item bg-darker dropdown noti-dropdown">
                        <a class="nav-link dropdown-toggle" id="notifications" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="venita-bell-icon"></i><?php if(!empty($total_count)){ ?><span class="notif-number"><?php echo $total_count; ?></span><?php } ?>
                        </a>
                        <?php if(!empty($notification_data)){ ?>
                        <div class="dropdown-menu" aria-labelledby="notifications" id="notifications_id">
                            <?php
                            foreach ($notification_data as $notification_data_row) { ?>
                            <div class="dropdown-item notificaton-individual">
                                <div class="notification-request-title">#VG<?php echo $notification_data_row->sr_id ?></div>
                                <div class="notification-title"><?php echo $notification_data_row->activity_type; ?><?php if(strpos($notification_data_row->activity_type, 'By') !== false || strpos($notification_data_row->activity_type, 'To') !== false)
                                        { 
                                            $activity_name =  $CI->get_assign_name($notification_data_row->activity_by);
                                            echo ' <span class="blue-text">'.$activity_name.'</span>'; } ?></div>
                                <div class="notification-info">
                                    <div class="date-n-time">on <?php echo date('m-d-Y, h:ia',strtotime($notification_data_row->dCreatedDate)); ?></div>
                                    <div class="status">status: <span class="blue-text"><?php echo $notification_data_row->sr_status; ?></span></div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                        <?php } ?>
                        </li>   
                        <?php } ?>
                        <?php if($this->session->userdata('user_type') == 'admin'){ ?> 
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url('admin'); ?>">Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url('admin/all-users'); ?>">All Users</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url('admin/create-user'); ?>">Create User</a>
                        </li>
                        <?php } ?>
                        <li class="nav-item dropdown bg-darker">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="venita-user-icon"></i> <?php if(!empty($this->session->userdata('user_id'))){ 

                                    $user_data = $CI->get_user_header_name($this->session->userdata('user_id'));

                                    $name = '';
                                    if(!empty($user_data))
                                    {
                                        $name = ucfirst($user_data->first_name).' '.ucfirst($user_data->last_name);
                                    }

                                    echo $name;
                                    } ?>
                            </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <?php if($this->session->userdata('user_type') == 'employee' || $this->session->userdata('user_type') == 'support'){ ?>
                                <a class="dropdown-item" href="<?php echo base_url('setting'); ?>">Settings</a>
                            <?php } ?>
                            <?php if($this->session->userdata('user_type') == 'admin'){ ?>
                                <a class="dropdown-item" href="<?php echo base_url('admin/setting'); ?>">Settings</a>
                            <?php } ?>
                            <a class="dropdown-item" href="<?php echo base_url('logout'); ?>">Logout</a>
                        </div>
                        </li>
                    </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <?php } ?>
<script type="text/javascript">
$(document).ready(function($) {
    $('#notifications_id').click(function(event) {
        disabledEventPropagation(event);
    });

    $('#noti_dropdown_id').click(function(event) {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>service_request/updatenotificationcount',
            dataType: "json",
            success: function(res) {
                if(res.result == "success"){
                    if(res.count > 0 || res.count =='')
                    {
                        $('.notif-number').remove();
                        //$('.notif-number').html(res.count);
                    }
                }
            }
        });
    });


    function disabledEventPropagation(event) {
      if (event.stopPropagation) {
        event.stopPropagation();
      } else if (window.event) {
        window.event.cancelBubble = true;
      }
    }
});
</script>
<script>
(function() {
        var current =window.location.href;
        current=current.split("?");
        current=current[0];
        // /console.log(current);
        $(".main-menu ul li").removeClass("active");
        $(".main-menu ul li a[href='" + current  + "']").parent("li").addClass("active"); 
        
        if (current.indexOf("setting") > -1) {
            $(".dropdown").addClass("active");
            $(".dropdown-menu a[href='" + current  + "']").addClass("active");
        }

})();
</script>