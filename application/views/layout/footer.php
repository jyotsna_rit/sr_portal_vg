<?php 
    if (!empty($this->session->userdata('user_id')))
    {
?>
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="footer-inner">
                <div class="social-sharing-links">
                    <ul>
                        <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/facebook.svg"></a></li>
                        <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/twitter.svg"></a></li>
                        <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/youtube.svg"></a></li>
                    </ul>
                </div>
                <div class="copyrights">
                    <p>&copy; <?php echo date('Y'); ?> Venita Group. All Rights Reserved.</p>
                </div>
                <div class="email-contact">
                    Email:<a href="mailto:help@venitagroup.com"> help@venitagroup.com</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php } ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/select2.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom-main.js"></script>


</body>
</html>