<!-- Main Content -->
    <?php
        $user_type = '';
        $category_ids = '';
        $service_request_ids = '';
        if(!empty($user_data) && !empty($user_data->user_type))
        {
            $user_type = $user_data->user_type;
           
            if(!empty($user_data->category_id) &&  $user_type == 'support')
            {
                $category_ids = $user_data->category_id;
            }

            $service_request_ids = '';
            if(!empty($user_data->service_request_id) &&  $user_type == 'support')
            {
                $service_request_ids = $user_data->service_request_id;
            }
        }
    ?>
    

    <div class="main-content content-with-mild-dark-bg settings-page">
        <div class="page-title title-center">
            <div class="container">
                <div class="back_title">
                <h3>Settings</h3>
                <a href="<?php if($this->session->userdata('back_url') !=''){ echo $this->session->userdata('back_url'); $this->session->unset_userdata('back_url');}else{ echo $this->agent->referrer(); } ?>" class="back-button"><i class="venita-long-back-arrow"></i>Back</a>
            </div>
            </div>
        </div>

        <!-- Create SR -->
    <div class="settings-wrap">
        
        <form id="settingfrm" method="post" action="" enctype="multipart/form-data">
            <input type="hidden" name="back_url" value="<?php echo $this->agent->referrer(); ?>">
            <div class="settings-profile-details create-user-box">
                <?php if(!empty($this->session->flashdata('success_msg'))){ ?>
                <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata('success_msg') ?></div>
                <?php } ?>
                <!-- <div class="error-msg"><?php echo $this->session->flashdata('error_msg'); ?></div>
                <div class="success-msg"><?php echo $this->session->flashdata('success_msg') ?></div> -->
                <h4>Profile Details</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">First Name</label>
                             <input type="text" class="form-control" name="first_name"  value="<?php if(!empty($user_data)) { echo $user_data->first_name; } ?>" placeholder="Enter First Name">
                            <?php echo form_error('first_name'); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Last Name</label>
                             <input type="text" class="form-control" name="last_name" value="<?php if(!empty($user_data)) { echo $user_data->last_name; } ?>" placeholder="Enter Last Name">
                            <?php echo form_error('last_name'); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Username</label>
                            <input type="hidden" value="<?php if(!empty($user_data->user_name)){ echo $user_data->user_name; } ?>" id="originaluser_name" />
                            <input type="text" class="form-control" name="user_name" value="<?php if(!empty($user_data)) { echo $user_data->user_name; } ?>" <?php if(!empty($user_data) && !empty($user_data->user_name)){ echo "readonly"; }?> placeholder="Enter Username">
                            <?php echo form_error('last_name'); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Email Address</label>
                            <input type="hidden" value="<?php if(!empty($user_data->emailid)){ echo $user_data->emailid; } ?>" id="originalemailid" />
                            <input type="email" class="form-control" name="emailid" value="<?php if(!empty($user_data)) { echo $user_data->emailid; } ?>" <?php if(!empty($user_data) && !empty($user_data->emailid)){ echo "readonly"; }?> placeholder="Enter Email Address">
                            <?php echo form_error('emailid'); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Location</label>
                            <select class="form-control" name="location_id">
                                <option value="">Select Location</option>
                                <?php
                                foreach($location_data as $location_row)
                                {
                                ?> 
                                <option value="<?php echo $location_row->location_id;?>" <?php if(!empty($user_data)) { if($user_data->location_id == $location_row->location_id) {echo "selected";} } ?>><?php echo $location_row->location_name;?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('location_id'); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Type of User<span>*</span></label>
                           <select class="form-control" name="user_type" id="user_type" disabled="disabled">
                                <option value="">Select type of user</option>
                                <option value="employee" <?php if(!empty($user_data)) { if($user_data->user_type =="employee") echo 'selected="selected"'; } ?>>Employee</option>
                                <option value="support" <?php if(!empty($user_data)) { if($user_data->user_type =="support") echo 'selected="selected"'; } ?>>Support</option>
                                <option value="admin" <?php if(!empty($user_data)) { if($user_data->user_type =="admin") echo 'selected="selected"'; } ?>>Admin</option>
                            </select>
                            <input type="hidden" name="user_type_hid" value="<?php if(!empty($user_data)) { echo $user_data->user_type; } ?>" />
                            <?php echo form_error('user_type'); ?>
                        </div>
                    </div>
                    <div id="hideshow" class="col-md-12" style="display:none;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">Category<span>*</span></label>
                                    <select class="form-control multiselect-box" name="category_id[]" id="category_id" <?php if(!empty($user_data) && $user_data->user_type == 'support'){ echo 'disabled'; } ?> multiple="multiple">
                                    <?php
                                    foreach($category_data as $category_row)
                                    {
                                    ?> 
                                    <option value="<?php echo $category_row->category_id;?>" <?php if(!empty($user_data)) { if($user_data->category_id == $category_row->category_id) {echo "selected";} } ?>><?php echo $category_row->category_name;?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" name="category_id" value="<?php if(!empty($user_data) && $user_data->category_id){ echo $user_data->category_id; } ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">Service Type<span>*</span></label>
                                    <select class="form-control multiselect-box" name="service_request_id[]" id="service_request_id" <?php if(!empty($user_data) && $user_data->user_type == 'support'){ echo 'disabled'; } ?> multiple="multiple">
                                    <?php
                                    if(!empty($user_data))
                                    {
                                        foreach($service_request_data as $service_request_row)
                                        {
                                    ?> 
                                    <option value="<?php echo $service_request_row->service_request_id;?>"><?php echo $service_request_row->service_request_name;?></option>
                                    <?php } } ?>
                                </select>
                                <input type="hidden" name="service_request_id" value="<?php if(!empty($user_data) && $user_data->service_request_id){ echo $user_data->service_request_id; } ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="submit-button">
                            <button type="submit" class="btn btn-block pink-btn">Update</button>
                        </div>
                    </div>
                </div>
                
            </div>
        </form>
        <form id="changepasswordfrm">
            <div class="settings-profile-details create-user-box">
                <div class="succesdiv"></div>
                <h4>Change Password</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Current Password<span>*</span></label>
                            <input type="password" class="form-control" name="sPassword" placeholder="Enter Current Password">
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">New Password<span>*</span></label>
                            <input type="password" class="form-control" name="sPasswordnew" id="sPasswordnew" placeholder="Enter New Password">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Confirm Password<span>*</span></label>
                            <div class="inp-toggle-eye">
                                <input type="password" class="form-control password-span" name="sConfirmPassword" id="sConfirmPassword" placeholder="********">
                                <span toggle="#password-field" class="fa fa-fw fa-eye field_icon toggle-password"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="submit-button">
                                <button type="submit" class="btn btn-block pink-btn">Change Password</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
<!-- /Main Content -->

<script type="text/javascript">
$(document).ready(function(){    
    
    // Get All Category
    var getall_cat = '<?php echo $category_ids; ?>';
    var vals = JSON.parse("[" + getall_cat + "]") ;

    if(getall_cat != '')
    {
        $('#category_id').val(vals).trigger("change");
    }

    // Get All Service Type
    var getall_service_request_id = '<?php echo $service_request_ids; ?>';
    var vals = JSON.parse("[" + getall_service_request_id + "]") ;

    if(getall_service_request_id != '')
    {
        $('#service_request_id').val(vals).trigger("change");
    }

    var user_type = '<?php echo $user_type; ?>';

    if(user_type !='' && user_type  == 'support')
    {
         $("#hideshow").show();
    }

    $('#user_type').bind('change', function(){
        var user_type = $('#user_type').find(":selected").val();
        if(user_type == 'support')
        { 
            $("#hideshow").show();
        }
        else
        { 
            $("#hideshow").hide();
        }
    });

    jQuery.validator.addMethod("noSpace", function(value, element) { 
        return value.indexOf(" ") < 0 && value != ""; 
    }, "");

     $("#settingfrm").validate({
        rules: {
            first_name: {
                required: true,
                isstring: true,
            },
            last_name: {
                required: true,
                isstring: true,
            },
            user_name:{
                required: true,
                noSpace: true,
                remote: {
                    param:{
                        url: "<?php echo base_url(); ?>admin/geteusername",
                        type: "post"
                    },
                    depends: function(element) {
                    // compare email address in form to hidden field
                    return ($(element).val() !== $('#originaluser_name').val());
                    }
                },
            },
            emailid:{
                required: true,
                email:true,
                remote: {
                    param:{
                        url: "<?php echo base_url(); ?>admin/getemailexists",
                        type: "post"
                    },
                    depends: function(element) {
                    // compare email address in form to hidden field
                    return ($(element).val() !== $('#originalemailid').val());
                    }
                },
            },
            location_id:{
                required: true,
            },
            user_type:{
                required: true,
            },
            <?php if(empty($user_data) && empty($user_data->password)){ ?>
            user_status:{
                required: true,
            },
            <?php } ?>
            'category_id[]': {
                required : function(){
                    var user_type= $('#user_type').find(":selected").val();
                    if((user_type== 'support')){
                        return true;
                    }
                    else{
                        return false;
                    }
                },
            },
            'service_request_id[]': {
                required : function(){
                    var user_type= $('#user_type').find(":selected").val();
                    if((user_type== 'support')){
                        return true;
                    }
                    else{
                        return false;
                    }
                },
            },
            
        },
        messages:{
            first_name: {
                required: 'Please enter first name.',
                isstring: 'Please enter valid first name.',
            },
            last_name: {
                required: "Please enter last name.",
                isstring: 'Please enter valid last name.',
            },
            user_name:{
                required: "Please enter user name.",
                noSpace: "Please enter valid username without space",
                remote: "User name is already exists.",
            },
            emailid:{
                required: "Please enter email address.",
                email: "Please enter valid email address.",
                remote: "Email address is already exists.",
            },
            location_id:{
                required: "Please select location.",
            },
            user_type:{
                required: "Please select user type.",
            },
            user_status:{
                required: "Please select status.",
            },
            'category_id[]': {
                required : "Please select category type.",
            },
            'service_request_id[]': {
                required : "Please select service request type.",
            },
        },
        errorPlacement: function(error, element) {
            if(element.attr("id") == "category_id" || element.attr("id") == "service_request_id"){
                error.insertAfter(element.next());  
            }
             else {
              error.insertAfter(element);
            }
        },
    });

    $("#changepasswordfrm").validate({
        rules: 
        {
            sPassword:
            {
                required:true,
                ispassword: true,
                remote: {
                    url: "<?php echo base_url(); ?>front_master/checkpassword",
                    type: "post"
                },  
            },
            sPasswordnew:
            {
                required:true,
                ispassword: true,  
            },
            sConfirmPassword:
            {
                required:true,
                ispassword: true,
                equalTo: '#sPasswordnew',
            },
        },
        messages: 
        {
            sPassword:
            {
                required:"Please enter current password.",
                ispassword: "Please enter minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.",
                remote: "Entered password is incorrect.",

            },
            sPasswordnew:
            {
                required:"Please enter new password.",
                ispassword: "Please enter minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.",    
            },
            sConfirmPassword:
            {
                required:"Please enter password.",
                ispassword: "Please enter minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.",
                equalTo:"New password and password doesn't match.",
            },  
        },
        submitHandler: function(form,e) {
            var sConfirmPassword = $('#sConfirmPassword').val();
            $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>front_master/update_password',
                data: { sConfirmPassword : sConfirmPassword
                },
                dataType : "json",
                success: function(res) {
                    if(res.result == "success"){   
                        if(res.change == "true")
                        {
                            document.getElementById("changepasswordfrm").reset();
                            $('.succesdiv').html('<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Password change successfully.</div>');
                        }
                    } 
                }
            }); 
            e.preventDefault();
        }
    });

});

</script>