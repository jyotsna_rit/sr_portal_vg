<?php 
    $CI =& get_instance();
    $user_type = $this->session->userdata('user_type');
?>

<style type="text/css">
    .disp-none {
        display: none;
    }
</style>
<!-- Main Content -->

<div class="main-content content-with-mild-dark-bg">
    <div class="page-title">
        <div class="container">
            <h3>My Service Requests</h3>
        </div>
    </div>

    <!-- Service Request listing Form -->
     <?php 
        $location_id = '';
        if(!empty($_GET['location_id'])) {
            $location_id =  $_GET['location_id'];
        }

        $sKeyword = '';
        if(!empty($_GET['sKeyword'])) {
            $sKeyword = $_GET['sKeyword'];
        }

        $request_date = '';
        if(!empty($_GET['request_date'])) {
            $request_date = $_GET['request_date'];
        }
    ?>
    <div class="service-request-listing-wrap">
        <div class="container">
            <div class="service-request-form-wrap">
                <form method="get">
                    <div class="service-request-search-filter-wrap">
                        <div class="service-request-form-inner">
                            <div class="service-request-form">
                                <div class="search-box">
                                    <i class="venita-search-icon"></i>
                                    <input type="text" name="sKeyword" value="<?php if(!empty($_GET['sKeyword'])) { echo $_GET['sKeyword']; } ?>" placeholder="Search by Title or Assigned To">
                                </div>
                                <div class="select-box">
                                        <select name="location_id">
                                        <option value="">Location</option>
                                        <?php
                                        foreach($location_data as $location_row)
                                        {
                                        ?> 
                                        <option value="<?php echo $location_row->location_id;?>" <?php if(!empty($_GET['location_id'])) { if ($location_id == $location_row->location_id){ echo "selected"; } } ?>><?php echo $location_row->location_name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="date-box">
                                    <input type="text" class="ui-datepicker-inp" name="request_date" value="<?php if(!empty($request_date)){ echo date('m/d/Y',strtotime($request_date));} ?>" placeholder="Select Requested Date">
                                </div>
                            </div>
                            <div class="submit-btn">
                                <button type="submit" class="btn pink-btn">Go</button>
                            </div>
                        </div>
                        <?php
                       
                            if (!empty($_GET['priority'])) {
                                $priority = $_GET['priority'];
                                $priority_send = "'".implode ( "','", $priority)."'";
                            } else {
                                $priority = array();
                                $priority_send = "";
                            }
                       
                        ?>
                        <div class="service-request-filter-wrap">
                            <div  id="priority" class="priprity-filter">
                                <div class="which-filter form-control">All Priority</div>
                                <div class="filter-dropdown">
                                    <label class="custom-checkbox">All Priority
                                        <input type="checkbox" name="priority[]" value="All" class="priority_cls" id="get_all" <?php if (!empty($priority)) {if (in_array('All', $priority)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Low Priority
                                        <input type="checkbox" name="priority[]" value="Low" class="priority_cls" <?php if (!empty($priority)) {if (in_array('Low', $priority)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Medium Priority
                                        <input type="checkbox" name="priority[]" value="Medium" class="priority_cls" <?php if (!empty($priority)) {if (in_array('Medium', $priority)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">High Priority
                                        <input type="checkbox" name="priority[]" value="High" class="priority_cls" <?php if (!empty($priority)) {if (in_array('High', $priority)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <div class="apply-btn">
                                        <button type="submit" class="apply-filter">apply</button>
                                    </div>
                                </div>
                            </div>

                            <?php
                                if (!empty($_GET['sr_status'])) {
                                    $sr_status = $_GET['sr_status'];
                                    $sr_status_send = "'".implode ( "','", $sr_status)."'";
                                }else {
                                    $sr_status = array();
                                    $sr_status_send = "";
                                }
                        ?>
                            <div id="sr_status" class="request-filter">
                                <div class="which-request-filter form-control">All Request</div>
                                <div class="filter-dropdown">
                                    <label class="custom-checkbox">All Request
                                        <input type="checkbox" name="sr_status[]" value="All" id="sr_status_all" class="sr_status_cls" <?php if (!empty($sr_status)) {if (in_array('All', $sr_status)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Open
                                        <input type="checkbox" name="sr_status[]" value="Open" class="sr_status_cls" <?php if (!empty($sr_status)) {if (in_array('Open', $sr_status)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Closed
                                        <input type="checkbox" name="sr_status[]" value="Closed" class="sr_status_cls" <?php if (!empty($sr_status)) {if (in_array('Closed', $sr_status)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">InProgress
                                        <input type="checkbox" name="sr_status[]" value="InProgress" class="sr_status_cls" <?php if (!empty($sr_status)) {if (in_array('InProgress', $sr_status)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <label class="custom-checkbox">Deleted
                                        <input type="checkbox" name="sr_status[]" value="Deleted" class="sr_status_cls" <?php if (!empty($sr_status)) {if (in_array('Deleted', $sr_status)) {echo 'checked';}}?>>
                                        <span class="custom-check"></span>
                                    </label>
                                    <div class="apply-btn">
                                        <button type="submit" class="apply-filter">apply</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<!-- Service Request listing -->

    <div class="service-request-listing">
        <div class="container">
             <?php if(!empty($this->session->flashdata('success_msg'))){ ?>
                <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata('success_msg') ?></div>
            <?php } ?>
            <?php if(!empty($this->session->flashdata('error_msg'))){ ?>
                <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata('error_msg') ?></div>
            <?php } ?>
            <div class="request-listing">
                 <table id="DataTable">
                    <thead class="disp-none">
                        <tr>
                        <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Main Content -->

<!--- Delete Popup --->
    <div class="modal fade delete-SR-modal" id="reopenRequestmodal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            Confirmation
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true"><i class="venita-times-icon"></i></span>
            </button>
          </div>
          <div class="modal-body">
            <p id="confirm_text">Are you sure want to reopen this Service Request?</p>
            <div class="yes-no-btns">
                <a href="#" id="yes_id" class="btn pink-btn" data-toggle="modal" data-target="#reopenRequestmodalDetails" data-dismiss="modal">Yes</a>
                <a href="#" class="btn blue-btn" data-toggle="modal" data-dismiss="modal">No</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--- Delete End --->

    <!---- Delete Confirm ---->
    <div class="modal fade delete-SR-modal-details" id="reopenRequestmodalDetails" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <span id="popup_line">Submit details to confirm service request reopening</span>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true"><i class="venita-times-icon"></i></span>
            </button>
          </div>
          <div class="modal-body">
            <form id="reopenfrm" method="post" action="<?php echo base_url('service_request/updatesrstatus'); ?>">
                <div class="form-group">
                    <label class="col-form-label">Reason<span></span></label>
                    <select class="form-control" name="reason">
                        <option value="">Select Valid Reason</option>
                        <option value="Request completed/resolved">Request completed/resolved</option>
                        <option value="Duplicate request">Duplicate request</option>
                        <option value="Invalid request">Invalid request</option>
                        <option value="Request put on-hold">Request put on-hold</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="col-form-label">Comment</label>
                    <textarea name="comment" placeholder="Enter Comment"></textarea>
                </div>
                <input type="hidden" name="sr_status" value="Open">
                <button type="submit" class="btn pink-btn">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>


<script type="text/javascript">
$(document).ready(function() {

    var priority = "<?php echo  $priority_send; ?>";
    var sr_status = "<?php echo  $sr_status_send; ?>";
    var location_id = '<?php echo $location_id; ?>';
    var sKeyword = '<?php echo $sKeyword; ?>';
    var request_date = '<?php echo $request_date; ?>';

    var t = $("#DataTable");
    if(t.length){
        var opt = {};
        opt.processing = true,
        opt.searching = false,
        opt.oLanguage= { 
            sProcessing: "", 
            sEmptyTable : "<div class='alert alert-warning'>No Request Found!</div>",
            oPaginate : {
            sPrevious : "<",    
            sNext : ">",
            } 
        },
        opt.serverSide = true,
        opt.bSort = false,    
        opt.info = false,
        opt.bJQueryUI= true,
        opt.lengthChange = false,
        opt.pageLength=10,
        //opt.pagingType= "input",
        opt.pagingType = "simple_numbers",
        opt.ajax = {
            url: '<?php echo base_url(); ?>service_request/getsrrequest',
            data:{
                priority:priority,sr_status:sr_status,location_id:location_id,sKeyword:sKeyword,request_date:request_date
            },
            complete: function() {
               getresize();
            },
            type:'POST',
            datatype : "application/json",
        },
        opt.fnDrawCallback =  function(oSettings) {
            if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
            $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
            }
            var rows = this.fnGetData();
            if (rows.length === 0 ) {
                $('body').addClass('no-table-record-found');
            }
        },
        t.dataTable(opt);
        $(t).on( 'processing.dt', function ( e, settings, processing ) {
            $(t).css( 'opacity', processing ? '0.7' : '1' );
        } )
        .dataTable();
    }


    $('#DataTable').on('click','.reopen-button', function (e) {
        $('.remove_sr_id').remove();
        var sr_id = $(this).data("id");
        console.log('Assigned'+sr_id);
        $('#reopenfrm').append('<input type="hidden" value="'+sr_id+'" class="remove_sr_id" name="sr_id">');
    });

    $("#reopenfrm").validate({
        rules: 
        {
            reason:
            {
                required:true,
            },
            comment:
            {
                required:true,
            },
        },
        messages: 
        {
            reason:
            {
                required:"Please select reason.",
            },
            comment:
            {
                required:"Please enter comment.",
            },
        },
    }); 
});
</script>
<script>
    $( function() {
        $(".ui-datepicker-inp").datepicker();
    });


   $('#get_all').on('click',function(){
        if(this.checked){
            $('.priority_cls').each(function(){
                this.checked = true;
            });
        }else{
             $('.priority_cls').each(function(){
                this.checked = false;
            });
        }
    });

   	$('#sr_status_all').on('click',function(){
        if(this.checked){
            $('.sr_status_cls').each(function(){
                this.checked = true;
            });
        }else{
             $('.sr_status_cls').each(function(){
                this.checked = false;
            });
        }
    });

    $(".which-filter").click(function(){
        if ($( "#priority").is(".show-dropdown"))
        { 
            $(".priprity-filter").removeClass("show-dropdown");
        }
        else
        {
            $(".priprity-filter").addClass("show-dropdown");
        }
    });
    $(".apply-filter").click(function(){
        $(".priprity-filter").removeClass("show-dropdown");
    });

    $('.priprity-filter').click(function(e){
        e.stopPropagation();
    });

    $('.request-filter').click(function(e){
        e.stopPropagation();
    });
    

    $(document).click(function(){
       $(".priprity-filter").removeClass("show-dropdown");
       $(".request-filter").removeClass("show-dropdown");
    });


    $(".which-request-filter").click(function(){
        if($( "#sr_status").is(".show-dropdown"))
        { 
            $(".request-filter").removeClass("show-dropdown");
        }
        else
        {
            $(".request-filter").addClass("show-dropdown");
        }

    });
    $(".apply-filter").click(function(){
        $(".request-filter").removeClass("show-dropdown");
    });


function getresize()
{
        var viewportWidth = $(window).width();
        if (viewportWidth < 768) {
            $(".request-individual").addClass("mobile-view-accordion");

            $(".request-title").click(function() {
                $(this).parent().toggleClass("mobile-view-accordion");
            });
        }
}
</script>