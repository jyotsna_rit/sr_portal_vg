<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<html>    
    <body style="background:#074578; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#ffffff;  font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
  <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
    <tbody>
      <tr>
        <td style="padding:20px 0 20px 0" align="center" valign="top">
          <table style="border:1px solid #E0E0E0;"  cellpadding="10" cellspacing="0" width="650">
            <tbody>
              <tr>
                <td align="center" style="border:1px solid #E0E0E0;" bgcolor="#ffffff" valign="top"><a href="<?php echo base_url(); ?>"><img src="<?php echo logo_url; ?>" alt="Venita Group" style="margin-bottom:5px;"  border="0" /></a>
                </td>
              </tr>
            <tr>
              <td style="text-align: center;border-top:1px solid #E0E0E0;border-bottom:1px solid #E0E0E0;background:#F3F3F3;"><span style="font-size: 26px; color:#333333;">Forgot Password</span></td>
              </tr>
            <tr>
             <td bgcolor="#ffffff" style="font-size:13px;line-height: 20px;">
              <strong>Dear <?php echo $mail_data['first_name']; ?>,</strong>
            </td>
            <tr>
              <td>
            	<p>We got a request to reset your Venita Group password. if this true, click below to reset your password.</p>
              </td>
            </tr>
            <tr>
              <td align="center" style="color: #fff;text-transform: uppercase;text-decoration: none;">
              <a style="background-color: #f94b91;display: inline-block;padding: 15px 20px;color: #fff;text-transform: uppercase;text-decoration: none;" href="<?php echo $mail_data['forgot_link']; ?>">Reset My Password</a>
              </td>
            </tr>
            <tr>
              <td>
              <p>If you ignore this message, your password won’t be changed.</p>
             </td>
           </tr>

            <tr><td align="center" bgcolor="#f94b91" style="border:1px solid #E0E0E0; font-size:12px; color:#ffffff; font-family:Verdana, Geneva, sans-serif;">Thank You,<b> Venita Group</b></td></tr>
            <tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
</div> 
</body>
</html>
 