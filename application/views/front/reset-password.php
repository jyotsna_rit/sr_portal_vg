<div class="login-page">
    <div class="login-box"></div>
    <div class="login-forgot-password-box">
        <div class="login-forgot-description">
            <div class="login-description">
                <h3>Reset Password</h3>
                <p>We will send you instructions to reset your password.</p>
            </div>
        </div>
        
        <div class="input-fields">
            <div class="logo">
                <img src="<?php echo base_url(); ?>assets/images/venita-logo.png" alt="logo">
            </div>
            <div class="login-fields">
                <form method="post" action=""  id="resetfrm">
                    <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('reset_user_id'); ?>">
                    <input type="hidden" name="user_type" value="<?php echo $this->session->userdata('reset_user_type'); ?>">
                    <div class="form-group">
                        <label class="col-form-label">Password<span>*</span></label>
                        <input type="password" class="form-control" name="password" value="<?php if(!empty($_POST)){ echo set_value('password'); } ?>" id="res-password" placeholder="New Password">
                        <?php echo form_error('password'); ?>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Confirm Password<span>*</span></label>
                        <div class="inp-toggle-eye">
                        <input type="password" class="form-control password-span" name="confirm_password" value="<?php if(!empty($_POST)){ echo set_value('confirm_password'); } ?>" placeholder="Confirm Password">
                        <span toggle="#password-field" class="fa fa-fw fa-eye field_icon toggle-password"></span>
                        </div>
                        <?php echo form_error('confirm_password'); ?>
                    </div>
                    <button type="submit" class="btn btn-block pink-btn">Reset</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    
    $("#resetfrm").validate({
        rules: {
            
            password:{
                required: true,
                ispassword: true,
            },
            confirm_password:{
                required: true,
                ispassword: true,
                equalTo: "#res-password",
            },
        },
        messages:{
            password:{
                required: "Please enter password.",
                ispassword: "Please enter minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.",
            },
            confirm_password:{
                required: "Please enter confirm password.",
                equalTo: "Password dose not match.",
                ispassword: "Please enter minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.",
            },
        },
        
        
        submitHandler: function(form) {
             form.submit();
        }
    });
    
         
});
</script>