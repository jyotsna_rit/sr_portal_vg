<div class="login-page">
    <div class="login-box"></div>
    <div class="login-forgot-password-box">
        <div class="login-forgot-description">
            <div class="login-description">
                <h3>LOGIN</h3>
                <p>To keep connected with us please login with username and password</p>
            </div>
            <div class="forgot-password-description">
                <h3>FORGOT PASSWORD?</h3>
                <p>We will send you instructions to reset your password.</p>
            </div>
        </div>
        
        <div class="input-fields">
            <div class="logo">
                <img src="<?php echo base_url(); ?>assets/images/venita-logo.png" alt="logo">
            </div>
            <?php if(!empty($this->session->flashdata('error_msg'))){ ?>
                <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata('error_msg') ?></div>
            <?php } ?>
            <div class="succesdiv"></div>
            <div class="errordiv"></div>
            <div class="login-fields">
                <form method="post" action="<?php echo base_url()?>"  id="loginfrm">
                    <input type="hidden" name="page_for" value="login">
                    <div class="form-group">
                        <label class="col-form-label">Username or Email Address</label>
                        <input type="text" class="form-control" name="user_name" value="<?php if(!empty($_POST)){ echo set_value('user_name'); }elseif(!empty($user_data)){ echo $user_data->user_name; } ?>" id="user_name" placeholder="Username">
                        <?php echo form_error('user_name'); ?>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Password</label>
                        <input type="password" class="form-control" name="password" value="<?php if(!empty($_POST)){ echo set_value('password'); }elseif(!empty($user_data) && get_cookie('remember_me') == 1){ echo safe_b64decode(get_cookie('password')); } ?>" placeholder="Password">
                        <?php echo form_error('password'); ?>
                    </div>
                    <div class="remember-forgot-password">
                        <div class="remember-me-div">
                            <label class="custom-checkbox">Remember me
                                <input type="checkbox" name="remember_me" value="1" <?php if(get_cookie('remember_me') == 1){ echo "checked"; }  ?>>
                                <span class="custom-check"></span>
                            </label>
                        </div>
                        <a href="javascript:void(0)" class="forgot-password-btn">Forgot Password ?</a>
                    </div>
                    <button type="submit" class="btn btn-block pink-btn">Log In</button>
                </form>
            </div>
            <div class="forgot-password-box">
                <form id="forgotpasswordfrm">
                    <div class="form-group">
                        <label class="col-form-label">Enter Username or Email Address</label>
                        <input type="text" class="form-control" name="forgot_user_name" id="forgot_user_name" placeholder="Enter Username or Email Address">
                        <?php echo form_error('forgot_user_name'); ?>
                    </div>
                    <button type="submit" class="btn btn-block pink-btn forgotbtn">Submit</button>
                    <div class="back-button-div">
                        <a href="javascript:void(0)" class="back-button"><i class="fas fa-angle-left"></i> Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    
    $("#loginfrm").validate({
        rules: {
            user_name:{
                required: true,
            },
            password:{
                required: true,
                //minlength: 8,
            },
            
        },
        messages:{
            user_name:{
                required: 'Please enter user name or email address.',
            },
            password:{
                required: "Please enter password.",
                //minlength: "Please enter minimum 8 characters.",
            },
        },
        // submitHandler: function(form) {
        //      form.submit();
        // }
    });

    // $("#user_name").on('keyup, keypress', function() {
    //     var el = document.getElementById("user_name");
    //     var val = el.value.replace(/\s/g, "");
    //     $("#user_name").val(val);
    // });


    $("#user_name").on('keyup', function() {
        var el = document.getElementById("user_name");
        var val = el.value.replace(/\s/g, "");
        $("#user_name").val(val);
    });

    $("#user_name").on('keypress', function() {
        var el = document.getElementById("user_name");
        var val = el.value.replace(/\s/g, "");
        $("#user_name").val(val);
    }); 

    $( "#forgot_user_name").on('keypress', function() {
        var el = document.getElementById("forgot_user_name");
        var val = el.value.replace(/\s/g, "");
        $("#forgot_user_name").val(val);
    });

    $( "#forgot_user_name").on('keyup', function() {
        var el = document.getElementById("forgot_user_name");
        var val = el.value.replace(/\s/g, "");
        $("#forgot_user_name").val(val);
    });

    $('.forgot-password-btn,.back-button').click(function(){
        $('.alert-success').remove();
        $('.alert-danger').remove();
    });

    $("#forgotpasswordfrm").validate({
        rules: {
            forgot_user_name:{
                required: true,
            },            
        },
        messages:{
            forgot_user_name:{
                required: 'Please enter user name or email address.',
            },
        },
        submitHandler: function(form,e) {
            var forgot_user_name = $('#forgot_user_name').val();
            $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>front/forgotpassword',
                data: { forgot_user_name : forgot_user_name
                },
                dataType : "json",
                success: function(res) {
                    if(res.result == "success"){
                        document.getElementById("forgotpasswordfrm").reset();   
                        if(res.mail_send == "true")
                        {
                            $('.succesdiv').html('<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your reset password link has been mailed to your email address.</div>');
                            //$.notify("Your password has been mailed to your email address", "success");
                        }
                        else if(res.mail_send == "false")
                        {
                            $('.errordiv').html('<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>your username or email address not register with us</div>');
                            //$.notify("your username or email address not register with us", "error");
                        }
                    } 
                }
            }); 
            e.preventDefault();
        }
    });

});
</script>